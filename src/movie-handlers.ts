import { Movie, IResult, IResultsOfResult } from './movie-interfaces';
import { movieMapper } from './mapper';

const randomInteger = (min: number, max: number): number => {
    const rand = min + Math.random() * (max + 1 - min);
    return Math.floor(rand);
};

const getResponse = async (
    url: URL,
    options: Record<string, string>
): Promise<Response> => {
    url.search = new URLSearchParams(options).toString();
    const response: Response = await fetch(url.href);
    return response;
};

export const getMovies = async (
    url: URL,
    options: Record<string, string>
): Promise<Array<Movie>> => {
    const response: Response = await getResponse(url, options);
    const result: IResult = await response.json();
    const movieObjList: Array<IResultsOfResult> = result.results;
    const movies: Array<Movie> = movieObjList.map(
        (movieObj: IResultsOfResult): Movie => movieMapper(movieObj)
    );

    return movies;
};

export const getMovie = async (
    url: URL,
    options: Record<string, string>
): Promise<Movie> => {
    const response: Response = await getResponse(url, options);
    const result: IResultsOfResult = await response.json();
    const movie: Movie = movieMapper(result);

    return movie;
};

export const getRandomMovie = (movies: Array<Movie>): Movie => {
    const randomMovieNumber: number = randomInteger(0, movies.length - 1);
    const randomMovie: Movie = movies[randomMovieNumber];
    return randomMovie;
};
