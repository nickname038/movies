export const apiKey: string = 'd9192c42e898d6fb6f9282bfa06e6759';

type apiKeys = 'SEARCH' | 'POPULAR' | 'TOP_RATED' | 'UPCOMING' | 'GET_INFO';

const apiPath: Record<apiKeys, string> = {
    SEARCH: '/search/movie',
    POPULAR: '/movie/popular',
    TOP_RATED: '/movie/top_rated',
    UPCOMING: '/movie/upcoming',
    GET_INFO: '/movie/*',
};

export const searchMovieUrl: URL = new URL(
    `https://api.themoviedb.org/3${apiPath.SEARCH}`
);

export const popularMovieUrl: URL = new URL(
    `https://api.themoviedb.org/3${apiPath.POPULAR}`
);

export const topRatedMovieUrl: URL = new URL(
    `https://api.themoviedb.org/3${apiPath.TOP_RATED}`
);

export const upcomingMovieUrl: URL = new URL(
    `https://api.themoviedb.org/3${apiPath.UPCOMING}`
);

export const getInfoUrlConstructor = (filmId: string): URL => new URL(
    `https://api.themoviedb.org/3${apiPath.GET_INFO.replace('*', filmId)}`
);
