import { showMovies } from './html-helper';
import { getMovies } from './movie-handlers';
import { Movie } from './movie-interfaces';
import { apiKey } from './data';

export const onPageChecked = async (
    url: URL,
    options: Record<string, string>
): Promise<Array<Movie>> => {
    options.api_key = apiKey;
    const movies: Array<Movie> = await getMovies(url, options);
    const isLoadMore: boolean = parseInt(options.page) !== 1;
    showMovies(movies, false, isLoadMore);
    return movies;
};
