import { Movie } from './../movie-interfaces';
import { topRatedMovieUrl } from './../data';
import { getRandomMovie } from './../movie-handlers';
import { showRandomMovie } from './../html-helper';
import { onPageChecked } from '../page-handlers';

const onChecked = async (page: number = 1): Promise<Array<Movie>> => {
    const options: Record<string, string> = {
        page: String(page),
    };

    const movies: Array<Movie> = await onPageChecked(topRatedMovieUrl, options);
    return movies;
};

export const loadMoreTopRated = (page: number): void => {
    onChecked(page);
};

export const topRated = async (
    setCurrentPage: CallableFunction
): Promise<void> => {
    const topRatedButton: HTMLInputElement = <HTMLInputElement>(
        document.getElementById('top_rated')
    );

    if (topRatedButton.checked) {
        setCurrentPage('topRated');
        const movies: Array<Movie> = await onChecked();
        const randomMovie: Movie = getRandomMovie(movies);
        showRandomMovie(randomMovie);
    }

    topRatedButton.addEventListener('change', () => {
        setCurrentPage('topRated');
        onChecked();
    });
};
