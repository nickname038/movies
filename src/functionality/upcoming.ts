import { Movie } from './../movie-interfaces';
import { upcomingMovieUrl } from './../data';
import { getRandomMovie } from './../movie-handlers';
import { showRandomMovie } from './../html-helper';
import { onPageChecked } from '../page-handlers';

const onChecked = async (page: number = 1): Promise<Array<Movie>> => {
    const options: Record<string, string> = {
        page: String(page),
    };

    const movies: Array<Movie> = await onPageChecked(upcomingMovieUrl, options);
    return movies;
};

export const loadMoreUpcoming = (page: number): void => {
    onChecked(page);
};

export const upcoming = async (
    setCurrentPage: CallableFunction
): Promise<void> => {
    const upcomingButton: HTMLInputElement = <HTMLInputElement>(
        document.getElementById('upcoming')
    );

    if (upcomingButton.checked) {
        setCurrentPage('upcoming');
        const movies: Array<Movie> = await onChecked();
        const randomMovie: Movie = getRandomMovie(movies);
        showRandomMovie(randomMovie);
    }

    upcomingButton.addEventListener('change', () => {
        setCurrentPage('upcoming');
        onChecked();
    });
};
