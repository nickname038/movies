import { Movie } from './../movie-interfaces';
import { localStorageHelper } from "./../localStorage-helper";
import { getInfoUrlConstructor, apiKey } from "../data";
import { getMovie } from "../movie-handlers";
import { showMovies } from './../html-helper';

type optionKeys = 'api_key';

export const favorites = (): void => {
    const favoritesTogglerButton: HTMLButtonElement = <HTMLButtonElement>(
        document.querySelector('button.navbar-toggler')
    );

    const options: Record<optionKeys, string> = {
        api_key: apiKey,
    };

    favoritesTogglerButton?.addEventListener('click', (): void => {
        const favorites: Array<string> = localStorageHelper.getFavorites();

        const favoritesList: Array<Promise<Movie>> = favorites.map(async (filmId: string): Promise<Movie> => {
        const url: URL = getInfoUrlConstructor(filmId);
        const movie: Movie = await getMovie(url, options);
        return movie;
        })

        Promise.all(favoritesList).then((movies :Array<Movie>): void => {
            showMovies(movies, true);
        })
    });
};
