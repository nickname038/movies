import { onPageChecked } from './../page-handlers';
import { searchMovieUrl } from './../data';

let searchMovieText: string;

const searchInput: HTMLInputElement = <HTMLInputElement>(
    document.getElementById('search')
);

const onSearch = async (page: number = 1): Promise<void> => {
    const options: Record<string, string> = {
        page: String(page),
        query: searchMovieText,
    };

    onPageChecked(searchMovieUrl, options);
};

export const loadMoreSearch = (page: number): void => {
    onSearch(page);
};

export const search = (setCurrentPage: CallableFunction): void => {
    const searchButton: HTMLButtonElement = <HTMLButtonElement>(
        document.getElementById('submit')
    );

    searchButton.addEventListener('click', () => {
        searchMovieText = searchInput.value;
        if (!searchMovieText) return;

        setCurrentPage('search');

        const checkedPageButton: HTMLInputElement = <HTMLInputElement>(
            document.querySelector('[checked]')
        );
        checkedPageButton.checked = false;

        onSearch();
    });
};
