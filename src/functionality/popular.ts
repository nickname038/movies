import { onPageChecked } from './../page-handlers';
import { Movie } from './../movie-interfaces';
import { popularMovieUrl } from './../data';
import { getRandomMovie } from './../movie-handlers';
import { showRandomMovie } from './../html-helper';

const onChecked = async (page: number = 1): Promise<Array<Movie>> => {
    const options: Record<string, string> = {
        page: String(page),
    };

    const movies: Array<Movie> = await onPageChecked(popularMovieUrl, options);
    return movies;
};

export const loadMorePopular = (page: number): void => {
    onChecked(page);
};

export const popular = async (
    setCurrentPage: CallableFunction
): Promise<void> => {
    const popularButton: HTMLInputElement = <HTMLInputElement>(
        document.getElementById('popular')
    );

    if (popularButton.checked) {
        setCurrentPage('popular');
        const movies: Array<Movie> = await onChecked();
        const randomMovie: Movie = getRandomMovie(movies);
        showRandomMovie(randomMovie);
    }

    popularButton.addEventListener('change', () => {
        setCurrentPage('popular');
        onChecked();
    });
};
