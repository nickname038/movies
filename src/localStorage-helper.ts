export const localStorageHelper = {
    getFavorites: (): Array<string> => {
        if (!localStorage.getItem('favorites')) return [];

        const favorites: Array<string> = <Array<string>>(
            JSON.parse(<string>localStorage.getItem('favorites'))
        );

        return favorites;
    },
    setFavorites: (favorites: Array<string>): void => {
        localStorage.setItem('favorites', JSON.stringify(favorites));
    },
};
