import { Movie } from './movie-interfaces';
import { localStorageHelper } from './localStorage-helper';

const filmContainer: HTMLElement = <HTMLElement>(
    document.getElementById('film-container')
);

const favoriteFilmContainer: HTMLElement = <HTMLElement>(
    document.getElementById('favorite-movies')
);

const movieCard = (
    movie: Movie,
    isFavorites: boolean = false
): HTMLDivElement => {
    const card: HTMLDivElement = document.createElement('div');
    card.classList.add('col-12', 'p-2');
    if (!isFavorites) card.classList.add('col-lg-3', 'col-md-4');
    card.dataset.id = String(movie.id);
    let isLiked: boolean = false;
    const favorites: Array<string> = localStorageHelper.getFavorites();
    if (favorites.includes(String(movie.id))) {
        isLiked = true;
    }

    let posterPath: string;

    if (movie.poster_path) {
        posterPath = `https://image.tmdb.org/t/p/original${movie.poster_path}`;
    } else {
        posterPath =
            'https://upload.wikimedia.org/wikipedia/commons/6/64/Poster_not_available.jpg';
    }

    card.innerHTML = `
    <div class="card shadow-sm">
        <img
            src="${posterPath}"
        />
        <svg
            xmlns="http://www.w3.org/2000/svg"
            stroke="red"
            fill="${isLiked ? 'red' : '#ff000078'}"
            width="50"
            height="50"
            class="bi bi-heart-fill position-absolute p-2"
            viewBox="0 -2 18 22"
        >
            <path
                fill-rule="evenodd"
                d="M8 1.314C12.438-3.248 23.534 4.735 8 15-7.534 4.736 3.562-3.248 8 1.314z"
            />
        </svg>
        <div class="card-body">
            <p class="card-text truncate">${movie.overview}</p>
            <div
                class="
                    d-flex
                    justify-content-between
                    align-items-center
                "
            >
                <small class="text-muted">${movie.release_date || '?'}</small>
            </div>
        </div>
    </div>
`;
    return card;
};

const randomMovieCard = (movie: Movie): HTMLDivElement => {
    const card: HTMLDivElement = document.createElement('div');
    card.classList.add('row', 'py-lg-5');

    if (movie.backdrop_path) {
        card.style.backgroundImage = `url(https://image.tmdb.org/t/p/original${movie.backdrop_path})`;
        card.style.backgroundPosition = 'center';
    }

    card.innerHTML = `
    <div
        class="col-lg-6 col-md-8 mx-auto"
        style="background-color: #2525254f"
    >
        <h1 id="random-movie-name" class="fw-light text-light">${movie.title}</h1>
        <p id="random-movie-description" class="lead text-white">
            ${movie.overview}
        </p>
    </div>
`;
    return card;
};

const likesHendler = (
    movieCardItems: Array<HTMLDivElement>,
    container: HTMLElement
): void => {
    movieCardItems.forEach((movieCard: HTMLDivElement): void => {
        const likeButtonElement: HTMLElement | null = movieCard.querySelector(
            '.bi.bi-heart-fill path'
        );

        likeButtonElement?.addEventListener('click', (event: Event): void => {
            const likeMark: SVGElement = <SVGElement>event.composedPath()[1];
            const likedFilmId: string = <string>movieCard.dataset.id;

            const favorites: Array<string> = localStorageHelper.getFavorites();

            if (likeMark.getAttribute('fill') === 'red') {
                const disLikedFilmIndex: number =
                    favorites.indexOf(likedFilmId);
                favorites.splice(disLikedFilmIndex, 1);
                localStorageHelper.setFavorites(favorites);

                likeMark.setAttribute('fill', '#ff000078');
            } else {
                favorites.push(likedFilmId);
                localStorageHelper.setFavorites(favorites);

                likeMark.setAttribute('fill', 'red');
            }

            if (container.id === 'favorite-movies') {
                const likedFilmInHomePage: HTMLElement | null =
                    filmContainer.querySelector(`[data-id="${likedFilmId}"]`);
                if (likedFilmInHomePage) {
                    const likeMark: SVGElement = <SVGElement>(
                        likedFilmInHomePage.querySelector('.bi.bi-heart-fill')
                    );
                    if (likeMark.getAttribute('fill') === 'red') {
                        likeMark.setAttribute('fill', '#ff000078');
                    } else {
                        likeMark.setAttribute('fill', 'red');
                    }
                }
            }
        });
    });
};

export const showMovies = (
    movies: Array<Movie>,
    isFavorites: boolean = false,
    isLoadMore: boolean = false
): void => {
    const container: HTMLElement = isFavorites
        ? favoriteFilmContainer
        : filmContainer;

    if (!isLoadMore) {
        container.innerHTML = '';
    }

    const movieCardItems: Array<HTMLDivElement> = [];

    movies.forEach((movie: Movie): void => {
        const movieCardItem: HTMLDivElement = movieCard(movie, isFavorites);
        container?.append(movieCardItem);
        movieCardItems.push(movieCardItem);
    });

    likesHendler(movieCardItems, container);
};

export const showRandomMovie = (movie: Movie): void => {
    const container: HTMLElement = <HTMLElement>(
        document.getElementById('random-movie')
    );

    container.innerHTML = '';
    container.append(randomMovieCard(movie));
};
