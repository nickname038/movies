export const movieFields: Array<string> = [
    'poster_path',
    'overview',
    'release_date',
    'id',
    'backdrop_path',
    'title',
];

export interface Movie {
    poster_path: string;
    overview: string;
    release_date: string;
    id: number;
    backdrop_path: string;
    title: string;
}

export type KMovie = keyof Movie;

export interface IResultsOfResult extends Movie {
    adult: boolean;
    genre_ids: Array<number>;
    total_results: number;
    original_language: string;
    original_title: string;
    popularity: number;
    poster_path: string;
    video: boolean;
    vote_average: number;
    vote_count: number;
}

export interface IResult {
    page: number;
    results: Array<IResultsOfResult>;
    total_pages: number;
    total_results: number;
}
