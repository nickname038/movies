import {
    Movie,
    KMovie,
    IResultsOfResult,
    movieFields,
} from './movie-interfaces';

export const movieMapper = (movieObj: IResultsOfResult): Movie => {
    let movie: any = {};
    Object.keys(movieObj).forEach((key: string): void => {
        if (movieFields.includes(key)) {
            movie[<KMovie>key] = movieObj[<KMovie>key];
        }
    });

    movie = <Movie>movie;
    return movie;
};
