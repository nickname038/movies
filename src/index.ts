import { favorites } from './functionality/favorites';
import { popular, loadMorePopular } from './functionality/popular';
import { search, loadMoreSearch } from './functionality/search';
import { topRated, loadMoreTopRated } from './functionality/top-rated';
import { upcoming, loadMoreUpcoming } from './functionality/upcoming';


type pageNames = 'popular' | 'topRated' | 'upcoming' | 'search';

let currentPage: pageNames;
let currentPageNumber: number = 1;

const setCurrentPage = (page: pageNames): void => { 
    currentPage = page;
    currentPageNumber = 1; 
};

const pageLoadMoreHandlers: Record<pageNames, CallableFunction> = {
    'popular': (pageNumber: number): void => loadMorePopular(pageNumber),
    'search': (pageNumber: number): void => loadMoreSearch(pageNumber),
    'topRated': (pageNumber: number): void => loadMoreTopRated(pageNumber),
    'upcoming': (pageNumber: number): void => loadMoreUpcoming(pageNumber),
}

export async function render(): Promise<void> {
    search(setCurrentPage);
    popular(setCurrentPage);
    topRated(setCurrentPage);
    upcoming(setCurrentPage);
    favorites();

    const loadButton: HTMLButtonElement = <HTMLButtonElement>document.getElementById('load-more');
    loadButton?.addEventListener('click', () => {
        pageLoadMoreHandlers[currentPage](++currentPageNumber);     
    });
}
